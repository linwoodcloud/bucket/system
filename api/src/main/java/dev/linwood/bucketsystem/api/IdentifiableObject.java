package dev.linwood.bucketsystem.api;

public interface IdentifiableObject {
    String getSlug();

    int getId();
}
